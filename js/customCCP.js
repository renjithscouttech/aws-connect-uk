function loadCCP() {
        //Initializing
        var alias = "everlightradiology-uk";
        var ccpUrl = 'https://' + alias + '.awsapps.com/connect/ccp#/';
        var getRoutingProfileAPI = 'https://3hazc80706.execute-api.eu-central-1.amazonaws.com/Dev';
        var postRoutingProfileAPI = 'https://6ofe0gnhc8.execute-api.eu-central-1.amazonaws.com/Dev/connectUpdateRoutingProfileUserUK ';

        //Connect CCP Integration
        var container = document.getElementById("ccpContainer");
        connect.core.initCCP(container, {
          ccpUrl: ccpUrl,
          softphone: {
            allowFramedSoftphone: true,
            disableRingtone: false
          }
        });
        window.isMuted = false;
        window.ccp = window.ccp || {};

        var c; connect.contact(function (contact) {
          c = contact;
          c.onConnecting(function (c) {
            var attr = c.getAttributes();
            var c1 = c.getConnections()[1];
            var c2 = c.getStatus();
            document.getElementById("contactID").value = c.contactId;
            document.getElementById("timeStamp").value = c2.timestamp;
            document.getElementById("phoneNumber").value = c1.getAddress()['phoneNumber'];
            if (attr.callReason) { document.getElementById("callReason").value = attr.callReason.value; }
            if (attr.clientName) { document.getElementById("clientName").value = attr.clientName.value; }
          });

          c.onRefresh((c) => {
            fillTable(c.getAttributes());
          });
        });

        var table;
        $(document).ready((a) => {
          table = $('#attributes').DataTable({
            columns: [
              { title: "Name" },
              { title: "Value" }
            ],
            paging: false,
            info: false,
            searching: false
          });
          $("#showAttributes").click(() => {
            $('#visibleAttributes').show();
            $('#hiddenAttributes').hide();
          });
          $("#hideAttributes").click(() => {
            $('#visibleAttributes').hide();
            $('#hiddenAttributes').show();
          });
        });

        var fillTable = (attributes) => {
          table.clear();
          for (var k in attributes) {
            var value = attributes[k].value;
            if (value.startsWith("http")) {
              value = '<a target="_blank" href="' + value + '">' + value + '</a>'
            }
            table.row.add([k, value]);
          }
          table.draw();
        };
        // Reset form after agent becomes available again
        connect.agent((agent) => {
          agent.onStateChange((event) => {
            if (event.newState === 'Available') {
              document.getElementById("contactDetails").reset();
              $('#attributes').DataTable().clear().draw();
            }
          });
        });

    //Set the agent to offline when browswer is closed
    window.onbeforeunload = function(e){
          goOffline();
    }
    window.addEventListener('unload', function (e) {
        // Cancel the event as stated by the standard.
        goOffline();
        // Chrome requires returnValue to be set.
        e.returnValue = '';
    });

    function closeLoginWindow() {
        loginWindow.close();
        $('#logoutButton').show();
    }

    //Subscribe to Agent Events from streams-API, and handle agent events
    connect.agent((agent) => {
        window.ccp.agent = agent;
        document.getElementById("agentName").innerHTML = agent.getName();
        document.getElementById("showQueueList").value = `Show ${agent.getName()}'s Queues`;
        document.getElementById("hideQueueList").value = `Hide ${agent.getName()}'s Queues`;
        getRoutingProfileLambda();/** Get Routing profile through Lambda */
        agent.onStateChange(agentStateChange);
        agent.onRefresh(agentRefresh);
        agent.onRoutable(agentRoutable);
        agent.onNotRoutable(agentNotRoutable);
        agent.onOffline(agentOffline);
    });

    function getRoutingProfileLambda() {
        var lambdaURL = getRoutingProfileAPI;
        var userDetails;
        var profileName = window.ccp.agent.getRoutingProfile().routingProfileId; //Added to default the profile name in the dropdown
        console.log('profileName is '+profileName);
        console.log('profileName 2 is '+window.ccp.agent.getRoutingProfile().name);
        $.get(lambdaURL, (data) => {
            console.log(lambdaURL);
            console.log(data);
            var select = $('#routingProfileSelect'),
                option,
                i = 0,
                optionData = data.data.RoutingProfileSummaryList;
            il = data.data.RoutingProfileSummaryList.length;
            userDetails = data.data.UserSummaryList;
            console.log('Option data before is '+optionData);
            var ukProfiles = filterItems(optionData, 'UK');
            ukProfiles  =ukProfiles.sort(function(a, b){
                var nameA=a.Name.toLowerCase(), nameB=b.Name.toLowerCase();
                if(nameA < nameB) return -1;
                if(nameA > nameB) return 1;
                return 0;
             })
            ukProfiles.map(function( _selectedProfile ){
                var profileOption = document.createElement('option');
                profileOption.setAttribute('value', _selectedProfile.Arn);
                profileOption.appendChild(document.createTextNode(_selectedProfile.Name));
                select.append(profileOption);
            });
            select.val(profileName);
        });

        function filterItems( _optionData, _query) {
            var returnObj = [];
            _optionData.map ( function ( option ) {
                if(option.Name.indexOf( _query ) === 0)
                    returnObj.push(option);
            });
            return returnObj;
        };

        $("#routingProfileSelect").on('change', $event => {
            var routingProfileId = $($event.currentTarget).val();
            var routingProfileName = $("#routingProfileSelect option:selected").text()
            var userName = window.ccp.agent.getConfiguration().username;
            console.log(window.ccp.agent.getConfiguration().username);
            console.log(routingProfileId);
            $.each(userDetails, function (i, v) {
                if (v.Username == userName) {
                    console.log('Inside if');
                    console.log(v.Id);
                    var userId = v.Id;
                    //alert(v.age);
                    var selectProfileURL = postRoutingProfileAPI;
                    $.post(selectProfileURL, { userId: userId, routingProfileId: routingProfileId })
                    .done(  res => {
                        console.log('After post');
                        console.log(routingProfileId);
                        console.log(res);
                        getRoutingProfileDetails(window.ccp.agent.getRoutingProfile());
                        console.log(window.ccp.agent.getRoutingProfile());
                    }).fail(err => {
                        console.log(err);
                    });
                }
            });
        })
    }

    function goAvailable() {
        var availableState = window.ccp.agent.getAgentStates().filter(function (state) {
            return state.type === connect.AgentStateType.ROUTABLE;
        })[0];
        var allStates = window.ccp.agent.getAgentStates();
        window.ccp.agent.setState(availableState, {
            success: function () {
                console.log("Set agent status to available via Streams");
            },
            failure: function (data) {
                console.log(data);
                console.log("Failed to set agent status to available via Streams");
            }
        });
    }

    function agentStateChange(agent) {
        console.log("in the agentStateChange function: Agent went from " + agent.oldState + " state to " + agent.newState);
        document.getElementById("agentStatus").innerHTML = agent.newState;
        if (agent.oldState === 'AfterCallWork' && agent.newState === 'Available') {
            document.getElementById("contactDetails").reset();
            document.getElementById("phoneNumber").innerHTML = "";
            $('#visibleQueueList').hide();
            $('#hiddenQueueList').show();
            $('#visibleAttributes').hide();
            $('#hiddenAttributes').show();
            $('#attributes').DataTable().clear().draw();
        }

        if (agent.newState === 'Available') {
            document.getElementById("contactDetails").reset();
            document.getElementById('phoneNumber').innerHTML = '';
            $('#visibleAttributes').hide();
            $('#hiddenAttributes').show();
            $('#attributes').DataTable().clear().draw();
        }

        if (agent.newState != 'PendingBusy' & agent.newState != 'Busy' & agent.newState != 'FailedConnectCustomer' & agent.newState != 'AfterCallWork' & agent.newState != 'MissedCallAgent') {
                var newState = window.ccp.agent.getAgentStates().filter(function (state) {
                return state.name === agent.newState;
            })[0];

            if (newState) {
                document.getElementById("agentRFDropdown").value = newState.agentStateARN;
                $('#agentRFDropdown').niceSelect('update');
            }
            document.getElementById("agentRFDropdown").value = newState.agentStateARN;
            $('#agentRFDropdown').niceSelect('update');
        }

        if (agent.newState == 'error' & agent.newState == 'FailedConnectCustomer') {
            goAvailable();
        }

        //Set the agent back to available from missedcall after 2 minutes
        if (agent.newState == 'MissedCallAgent')
        {
          console.log ("agent.isSoftphoneEnabled() 1 ");
          setTimeout(function(){ goAvailable(); }, 30000);
        }
    }

    function agentRefresh(agent) {
        getRoutingProfileDetails(agent.getRoutingProfile());
        console.log("in the agentRefresh function: " + agent.getName() + " has refreshed");
    }

    function getRoutingProfileDetails(routingProfile) {
        var queueList = routingProfile.queues;
        fillQueuesTable(queueList);
    }

    function goOffline() {
        var offlineState = window.ccp.agent.getAgentStates().filter(function (state) {
            return state.type === connect.AgentStateType.OFFLINE;
        })[0];
        window.ccp.agent.setState(offlineState, {
            success: function () {
                console.log("Set agent status to Offline via Streams");
            },
            failure: function () {
                console.log("Failed to set agent status to Offline via Streams");
            }
        });
    }

    function changeAgentState(myStateARN) {
        var newState = window.ccp.agent.getAgentStates().filter(function (state) {
            return state.agentStateARN === myStateARN;
        })[0];
        window.ccp.agent.setState(newState, {
            success: function () {
                console.log("Set agent status to available via Streams");
            },
            failure: function (data) {
                console.log(data);
                console.log("Failed to set agent status to available via Streams");
            }
        });
    }

    $('#agentRFDropdown').change(function () {
        //Use $option (with the "$") to see that the variable is a jQuery object
        var $option = $(this).find('option:selected');
        var stateARN = $option.val(); //to get content of "value" attrib
        var stateName = $option.text(); //to get <option>Text</option> content
        changeAgentState(stateARN);
    });

    //Subscribe to Contact Events from streams-API, and handle Contact  events
    connect.contact((contact) => {
        window.ccp.contact = contact;
        console.log("Subscribing to events for contact");
        if (contact.getActiveInitialConnection() && contact.getActiveInitialConnection().getEndpoint()) {
            console.log("New contact is from " + contact.getActiveInitialConnection().getEndpoint().phoneNumber);
        } else {
            console.log("This is an existing contact for this agent");
        }
        contact.onIncoming(contactIncoming);
        contact.onConnecting(contactConnecting);
        contact.onAccepted(contactAccepted);
        contact.onConnected(contactConnected);
        contact.onEnded(contactEnded);
        contact.onRefresh(contactRefreshed);
    });

    function contactIncoming(contact) {
        console.log("incoming contact show the answer button");
    }

    function contactRefreshed(contact) {
        console.log("contact refreshed");
        getContactAttributes(contact);
    }

    function contactConnecting(contact) {
        var c1 = contact.getConnections()[1];
        var isInbound = contact.isInbound();
        document.getElementById("contactID").value = contact.contactId;
        document.getElementById("phoneNumber").innerHTML = c1.getAddress().phoneNumber;
        getContactAttributes(contact);
        console.log(`isInbound value is ${isInbound}`);
    }

    //Create table to show attributes
    var attributesTable;
    $(document).ready((a) => {
        attributesTable = $('#attributes').DataTable({
            columns: [{
                title: "Name"
            },
            {
                title: "Value"
            }
            ],
            paging: false,
            info: false,
            searching: false
        });
        $("#showAttributes").click(() => {
            $('#visibleAttributes').show();
            $('#hiddenAttributes').hide();
        });
        $("#hideAttributes").click(() => {
            $('#visibleAttributes').hide();
            $('#hiddenAttributes').show();
        });
    });

    //Populate attribute table with new contact attributes
    var fillAttributesTable = (attributes) => {
        attributesTable.clear();
        for (var k in attributes) {
            var value = attributes[k].value;
            if (value.startsWith("http")) {
                value = '<a target="_blank" href="' + value + '">' + value + '</a>';
            }
            attributesTable.row.add([k, value]);
        }
        attributesTable.draw();
    };

    //Create table to show assigned queues
    var queueTable;
    $(document).ready((a) => {
        queueTable = $('#queueList').DataTable({
            columns: [{
                title: "Queue Name:"
            }],
            paging: false,
            info: false,
            searching: false
        });
        $("#showQueueList").click(() => {
            $('#visibleQueueList').show();
            $('#hiddenQueueList').hide();
        });
        $("#hideQueueList").click(() => {
            $('#visibleQueueList').hide();
            $('#hiddenQueueList').show();
        });
    });

    //Populate queue table with agent queues from routing profile
    var fillQueuesTable = (queueList) => {
        queueTable.clear();
        for (var k in queueList) {
            var queueName = queueList[k].name;
            queueTable.row.add(["&nbsp&nbsp&nbsp" + queueName]);
        }
        queueTable.draw();
    };
}

$(document).ready(function () {
    console.log("call loadCCP function");
    loadCCP();
});
